import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {AppComponent} from './app.component';
import {PaymentDetailsComponent} from './payment-details/payment-details.component';
import {PaymentDetailListComponent} from './payment-details/payment-detail-list/payment-detail-list.component';
import {PaymentDetailComponent} from './payment-details/payment-detail/payment-detail.component';
import {PaymentDetailService} from './shared/payment-detail.service';
import {from} from 'rxjs';
import { LayoutComponent } from './layout/layout.component';
import {HeaderComponent} from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { WelcomeComponent } from './layout/welcome/welcome.component';

@NgModule({
  declarations: [
    AppComponent,
    PaymentDetailsComponent,
    PaymentDetailListComponent,
    PaymentDetailComponent,
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
    WelcomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [PaymentDetailService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
